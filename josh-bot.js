require('dotenv').config();

const Discord = require('discord.js');
const client = new Discord.Client();

const bot_secret_token = process.env.BOT_TOKEN;
let channelId = null;

const availableCmds = ['one', 'two', 'three'];

client.on('ready', () => {
    console.log("Connected as " + client.user.tag)

    client.user.setActivity("with myself", {
        type: "STREAMING"
    });

    client.guilds.forEach(guild => {
        console.log(" - " + guild.name);
        guild.channels.forEach(channel => {
            if (channel.name == 'general' && channel.type == 'text') {
                channelId = channel.id;
            }
        });
    });

    const generalChannel = client.channels.get(channelId);
    generalChannel.send("Hello world!");
});

client.on('message', receivedMessage => {
    if (receivedMessage.author === client.user) {
        return;
    }

    if (receivedMessage.content.startsWith('%')) {
        processCommand(receivedMessage);
    }
});

function processCommand(receivedMessage) {
    const fullCommand = receivedMessage.content.substr(1);
    const splitCommand = fullCommand.split(" ");
    const primaryCommand = splitCommand[0];
    const arguments = splitCommand.slice(1);

    console.log("Command received: " + primaryCommand);
    console.log("Arguments: " + arguments);

    if (primaryCommand === 'help') {
        helpCommand(receivedMessage);
    } else {
        receivedMessage.channel.send("I don't understand you");
    }
}

function helpCommand(receivedMessage) {
    receivedMessage.channel.send(
        receivedMessage.author.toString() + "\nAvailable commands: " + availableCmds.toString()
    );
}

client.login(bot_secret_token);